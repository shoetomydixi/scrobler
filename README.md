Hi there guys.

This is scrobbler to parse https://news.ycombinator.com/


I decide to add to more flexibility to this scrobbler   
to have ability parse sites with rich content (for exmaple JS-block or sites which renders with JS)

So I used phantomJS (headless browser) as engine to get content from site.   
PhantomJS can be easily replace with request library for example.

We have two main libraries there, in lib folder   
phantomProcesser.js - library which helps to get info from site   
htmlProcessor.js - library which helps to get parsed array form html string
 
To run parser with docker you need to execute next commands:   
docker build -t <your username>/scrobbler .   
and then   
docker run -p 49160:8080 -d <your username>/scrobbler   

Also you can run parser with command:
npm start --posts 100   

To run unit tests:
mocha ./tests/htmlProcessor.spec.js

List of libraries:

PhantomJS - engine which I used to get data from sites    
Async - collection of algoritms to process flow of data handling    
Cheerio - JQuery core at backend, to simplify HTML parsing in UI styel   
Lodash - collection of alghoritms to process data    
q - Implementation of promises    