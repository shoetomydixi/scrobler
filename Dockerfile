FROM nodesource/jessie:0.12.9
RUN rm /bin/sh && ln -s /bin/bash /bin/sh

RUN echo "Scrobbler"

# Create app directory
RUN mkdir -p /usr/src/scrobbler
WORKDIR /usr/src/scrobbler

RUN echo "Update system"

# Update system
RUN apt-get update -y
RUN apt-get upgrade -y

RUN apt-get install -y libfontconfig

RUN npm install -g nodemon
RUN npm install -g mocha

COPY package.json /usr/src/scrobbler/

RUN npm install --quiet --production

COPY . /usr/src/scrobbler/

RUN node -v

EXPOSE 8080

CMD [ "npm", "start" ]