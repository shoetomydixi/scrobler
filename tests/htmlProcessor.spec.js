/**
 * Created by deniskashuba on 20.09.16.
 */

'use strict';

var sinon = require('sinon');

var expect = require('chai').expect;
var assert = require('chai').assert;
var htmlProcessor = require('../lib/htmlProcessor');

var HtmlProcessor = new htmlProcessor();

describe('DifferenceMapper', function () {

	beforeEach(function () {



	});

	describe(' - Check html parser for https://news.ycombinator.com/', function () {

		it('should return cheerio object', function () {

			var htmlString = '<table><tr></tr></table></table>';

			expect(HtmlProcessor.parseToDom(htmlString)).to.have.any.keys('load', 'html', 'xml', 'contains');

		});

		it('should return array', function () {

			var stringWithNumber = 'asdf 120 dfsa';

			expect(HtmlProcessor.getNum(stringWithNumber)).to.be.instanceof(Array);

		});

		it('should return first number from string with number', function () {

			var stringWithNumber = 'asdf 120 dfsa 220';

			expect(HtmlProcessor.getNum(stringWithNumber)).to.include('120');

		});

		it('should return array with elements with specified className', function () {

			var htmlString = '<ul><li><a class="testClass">test</a></li><li><a class="testClass">test2</a></li></ul>';

			var $ = HtmlProcessor.parseToDom(htmlString);

			expect(HtmlProcessor.getItemsByClass($, htmlString, 'testClass')).to.have.deep.property('length', 2);

		});

	});

});
