/**
 * Created by deniskashuba on 20.09.16.
 */

var cheerio = require('cheerio');
var async = require('async');
var _ = require('lodash');
var Q = require('q');

/**
 *
 * Html processor
 *
 * @constructor
 */
var HtmlProcessor = function () {

	var self = this;

	/**
	 *
	 * Entry point for html parser
	 *
	 * @param htmlString
	 * @param callback
	 * @returns {*|promise}
	 */
	this.parseHtml = function (htmlString, callback) {

		var deferred = Q.defer();

		var $ = self.parseToDom(htmlString);

		var items = self.getItemsByClass($, htmlString, 'storylink');

		var firstPartArr = [];
		var secondPartArr = [];

		self.formatTitleAndLink(items).then(function(resultArr) {

			firstPartArr = resultArr;

			var items = self.getItemsByClass($, htmlString, 'subtext');
			return self.formatInfo(items);

		}).then(function(resultArr) {

			secondPartArr = resultArr;

			if (firstPartArr.length === secondPartArr.length) {

				var mergedArray = _.merge(firstPartArr, secondPartArr);

				callback(mergedArray);

			}else {

				console.log('arrays length missmatch, something went worng!');

			}

		});

		return deferred.promise;

	};

	/**
	 *
	 * Get DOM from HTML string
	 *
	 * @param htmlString
	 */
	this.parseToDom = function (htmlString) {

		return cheerio.load(htmlString);

	}

	/**
	 *
	 * Handler to parse number
	 *
	 * @param string
	 * @returns {*|Array|{index: number, input: string}}
	 */
	this.getNum = function(string) {

		return string.match(/\d+/g);

	}

	/**
	 *
	 * get items by className
	 *
	 * @param $
	 * @param htmlString
	 * @param className
	 * @returns {*|jQuery|HTMLElement}
	 */
	this.getItemsByClass = function ($, htmlString, className) {

		return $('.' + className);

	}

	/**
	 *
	 * Entry point to format info array
	 *
	 * @param arr
	 * @returns {*|promise}
	 */
	this.formatInfo = function (arr) {

		var deferred = Q.defer();

		var infoArr = [];

		async.eachSeries(arr, function (item, callback) {

			var parsedObj = self.getInfo(item);

			if (Object.keys(parsedObj).length)
				infoArr.push(parsedObj);

			callback();

		}, function () {

			deferred.resolve(infoArr);

		});

		return deferred.promise;

	};

	/**
	 *
	 * Entry point to format title and link array
	 *
	 * @param arr
	 * @returns {*|promise}
	 */
	this.formatTitleAndLink = function (arr) {

		var deferred = Q.defer();

		var linkArr = [];

		async.eachSeries(arr, function (item, callback) {

			var parsedObj = self.getTitleAndLink(item);

			if (Object.keys(parsedObj).length)
				linkArr.push(parsedObj);

			callback();

		}, function () {

			deferred.resolve(linkArr);

		});

		return deferred.promise;

	};

	/**
	 *
	 * Handle DOM data to parse info array
	 *
	 * @param infoItem
	 * @returns {{}}
	 */
	this.getInfo = function(infoItem) {

		var objToRet = {};

		if (infoItem && infoItem.children && infoItem.children.length) {

			for (var index in infoItem.children) {

				var item = infoItem.children[index];

				if (item && item.attribs) {

					if (item.attribs.class === 'score') {

						if (item.children && item.children.length) {

							var thenumArr = self.getNum(item.children[0].data);

							if (thenumArr && thenumArr.length)
								objToRet.points = thenumArr[0];

						}

					}else if (item.attribs.class === 'hnuser') {

						if (item.children && item.children.length) {

							objToRet.author = item.children[0].data;

						}

					}

				}

			}

			var itemWithComment = infoItem.children[infoItem.children.length -2];

			if (itemWithComment && itemWithComment.children && itemWithComment.children.length) {

				if (itemWithComment.children[0].data.toLowerCase().indexOf("comments") >= 0) {
					var thenumArr = self.getNum(itemWithComment.children[0].data);
					objToRet.comment = thenumArr[0];
				}else {
					objToRet.comment = 0;
				}

			}

		}

		return objToRet;

	};

	/**
	 *
	 * Handle DOM data to parse title and link array
	 *
	 * @param item
	 * @returns {{}}
	 */
	this.getTitleAndLink = function (item) {

		var objToRet = {};

		if (item) {

			if (item.attribs) {

				objToRet.uri = item.attribs.href;

			}

			if (item.children && item.children.length) {

				objToRet.title = item.children[0].data;

			}

		}

		return objToRet;

	}

};

module.exports = HtmlProcessor;

