/**
 * Created by deniskashuba on 20.09.16.
 */

var phantom = require('phantom');
var async = require('async');
var htmlProcessor = require('./htmlProcessor');

var HtmlProcessor = new htmlProcessor();

/**
 *
 * Phantom processor
 *
 * @constructor
 */
var PhantomProcessor = function () {

	var self = this;

	var pageToScrab = 'https://news.ycombinator.com/';
	var itemsToGet = 30;
	var countPerPage = 30;
	var countOfLoops = 0;
	var globalPh = null;

	var printedArray = [];

	/**
	 *
	 * Entry point for processor
	 *
	 * @param itemsNum
	 * @param pageUrl
	 */
	this.runFlow = function(itemsNum, pageUrl) {

		itemsToGet = itemsNum;

		if (pageUrl && typeof pageUrl !== 'undefined' && pageUrl !== '')
			pageToScrab = pageUrl;

		// calculate count of requests
		countOfLoops = Math.ceil(itemsToGet / countPerPage);

		phantom.create().then(function(ph) {

			globalPh = ph;

			self.createPage(1);

		});

	};

	/**
	 *
	 * Create page
	 *
	 * @param pageNum
	 */
	this.createPage = function(pageNum) {

		globalPh.createPage().then(function(page) {

			self.openPage(page, pageNum);

		});

	};

	/**
	 *
	 * Open page
	 *
	 * @param pageNum
	 */
	this.openPage = function(page, pageNum) {

		if (pageNum > countOfLoops) {

			console.log('we are done');
			this.printResults(printedArray);

		}else {

			var pageToOpen = pageToScrab;

			if (pageNum > 1)
				pageToOpen = pageToScrab + 'news?p=' + pageNum;

			page.open(pageToOpen).then(function(status) {
				console.log('status ' + status);
				self.getContent(page, pageNum);
			});

		}

	};

	/**
	 *
	 * Get page content
	 *
	 * @param pageNum
	 */
	this.getContent = function(page, pageNum) {

		page.property('content').then(function(content) {

			HtmlProcessor.parseHtml(content, function(resultArr) {

				printedArray = printedArray.concat(resultArr);

				pageNum++;

				self.createPage(pageNum);

			});

		});

	};

	/**
	 *
	 * Print results and cut printed Array
	 *
	 * @param pageNum
	 */
	this.printResults = function(arrayToPrint) {

		var cuttedArray = arrayToPrint.slice(0, itemsToGet);

		console.log('-=-=-=-=-=-=-=-=- Length of printed array -=-=-=-=-=-=-=-=- ');
		console.log(cuttedArray.length);
		console.log(cuttedArray);
		console.log('-=-=-=-=-=-=-=-=- Length of printed array -=-=-=-=-=-=-=-=- ');
		console.log(cuttedArray.length);

	}

};

module.exports = PhantomProcessor;
