/**
 * Created by deniskashuba on 20.09.16.
 */

var phantomProcessor = require('./lib/phantomProcessor.js');

var pageToScrab = 'https://news.ycombinator.com/';

var defaultCount = 100,
	count = 0;

/**
 *
 * Handle incoming parameters
 *
 */
try {

	var parsedObj = JSON.parse(process.env.npm_config_argv);
	count = (parsedObj.remain && parsedObj.remain.length) ? parseInt(parsedObj.remain[0]) : defaultCount;

	if (count > defaultCount || count <= 0)
		count = defaultCount;

}catch(e) {

	console.log('cant parse input arguments, we will use default count ' + defaultCount);

}

/**
 *
 * Run scrobbler here
 *
 */
var PhantomProcessor = new phantomProcessor();
PhantomProcessor.runFlow(count, pageToScrab);